package edu.eam.ingesoft.courses.controllers

import CommonTests
import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.repositories.CourseProfessorLogRepository
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class CourseProfessorLogControllerTest : CommonTests() {

    @Autowired
    protected lateinit var courseProfessorLogRepository: CourseProfessorLogRepository

    @Test
    fun editLog() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testData/courseControllerTest/courseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testData/courseControllerTest/courseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{    
               "description": "Description FILO CRACK",
                "hours": 4,
                "created_at": "2021-04-13 05:00:00",
                "log_date": {
                "hour": 11,
                "minute": 0
            },
                "day_of_week" : "TUESDAY"
            }"""

        val request = MockMvcRequestBuilders.put("${Routes.PATH_COURSES}/${Routes.EDIT_PROFESSOR_LOG}", "2020", "1")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = courseProfessorLogRepository.findById("1").get()

        Assertions.assertEquals("Description FILO CRACK", professors.description)
        Assertions.assertEquals(4, professors.hours)
        Assertions.assertEquals(11, professors.logDate?.hour)
        Assertions.assertEquals(0, professors.logDate?.minute)
    }

    @Test
    fun editProfessorLogNotFound() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testData/courseControllerTest/courseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testData/courseControllerTest/courseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{    
               "description": "Description FILO CRACK",
                "hours": 4,
                "created_at": "2021-04-13 05:00:00",
                "log_date": {
                "hour": 11,
                "minute": 0
            },
                "day_of_week" : "TUESDAY"
            }"""

        val request = MockMvcRequestBuilders.put("${Routes.PATH_COURSES}/${Routes.EDIT_PROFESSOR_LOG}", "2020", "6000")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Professor Log not found")))
    }

    @Test
    fun editCoursesNotFound() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testData/courseControllerTest/courseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testData/courseControllerTest/courseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{    
               "description": "Description FILO CRACK",
                "hours": 4,
                "created_at": "2021-04-13 05:00:00",
                "log_date": {
                "hour": 11,
                "minute": 0
            },
                "day_of_week" : "TUESDAY"
            }"""

        val request = MockMvcRequestBuilders.put("${Routes.PATH_COURSES}/${Routes.EDIT_PROFESSOR_LOG}", "3520", "1")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course is not found")))
    }

    @Test
    fun editIsCheckedTrue() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testData/courseControllerTest/courseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testData/courseControllerTest/courseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{    
               "description": "Description FILO CRACK",
                "hours": 4,
                "created_at": "2021-04-13 05:00:00",
                "log_date": {
                "hour": 11,
                "minute": 0
            },
                "day_of_week" : "TUESDAY"
            }"""

        val request = MockMvcRequestBuilders.put("${Routes.PATH_COURSES}/${Routes.EDIT_PROFESSOR_LOG}", "2020", "2")
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Checked is true")))
    }

    @Test
    fun delete() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DELETE_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testData/courseControllerTest/courseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testData/courseControllerTest/courseProfessorLogControllerTest/courses.json")

        val request = MockMvcRequestBuilders.delete("${Routes.PATH_COURSES}/${Routes.DELETE_PROFESSOR_LOG}", "2020", "1")
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val professors = courseProfessorLogRepository.findAll().asSequence().toList()

        val toAssert = professors.find { it.id == "1" }

        Assert.assertNull(toAssert)
    }

    @Test
    fun createProfessorLog() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testDatacourseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
            "description" : "1",
            "hours" : 11,
            "created_at" : "2021-04-12 05:00:00",
            "log_date" :  {
            "hour": 11,
            "minute": 0
            },
            "day_of_week" : "TUESDAY" 
            }"""

        val request =
            MockMvcRequestBuilders.post("${Routes.PROFESSORS_PATH}/${Routes.PATH_CREATE_PROFESSOR_LOG}", "2021", "2")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun createProfessorLogAndDateIsNotCourse() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testDatacourseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "description" : "1",
        "hours" : 2,
        "created_at" : "2021-04-12 05:00:00",
        "log_date" :  {
        "hour": 1,
        "minute": 0
        },
        "day_of_week" : "TUESDAY" 
        }"""

        val request =
            MockMvcRequestBuilders.post("${Routes.PROFESSORS_PATH}/${Routes.PATH_CREATE_PROFESSOR_LOG}", "2021", "2")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This log date does not match with the selected course")))
    }

    @Test
    fun createProfessorLogAndCourseIsNotFount() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testDatacourseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "description" : "1",
        "hours" : 2,
        "created_at" : "2021-04-12 05:00:00",
        "log_date" :  {
        "hour": 11,
        "minute": 0
        },
        "day_of_week" : "TUESDAY" 
        }"""

        val request =
            MockMvcRequestBuilders.post("${Routes.PROFESSORS_PATH}/${Routes.PATH_CREATE_PROFESSOR_LOG}", "2023", "1")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This course was not found")))
    }

    @Test
    fun createProfessorLogAndLogIsSameHour() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_TEACHER_LOG),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_professor_logs", "/testDatacourseProfessorLogControllerTest/professorLog.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "description" : "1",
        "hours" : 10,
        "created_at" : "2021-04-12 05:00:00",
        "log_date" :  {
        "hour": 10,
        "minute": 0
         },
        "day_of_week" : "TUESDAY" 
        }"""

        val request =
            MockMvcRequestBuilders.post("${Routes.PROFESSORS_PATH}/${Routes.PATH_CREATE_PROFESSOR_LOG}", "2021", "2")
                .content(jsonBody)
                .contentType(MediaType.APPLICATION_JSON)
                .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("ProfessorLog is in this already")
                )
            )
    }
}
