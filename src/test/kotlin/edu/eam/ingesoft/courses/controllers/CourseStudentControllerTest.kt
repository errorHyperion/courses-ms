package edu.eam.ingesoft.courses.controllers

import CommonTests
import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

class CourseStudentControllerTest : CommonTests() {

    @Test
    fun createCourseStudentFailureTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_FAILURE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "not_attendance_at" :{
            "hour": "7",
            "minute" : "41"
        },
        "day_of_week":"TUESDAY",
        "created_at": "2021-04-10 05:00:00"
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val courseStudentAttendanceToAssert = courseStudentRepository.findByStudentIdAndCourseId(studentId, courseId)
            ?.courseStudentAttendance?.find { it.quantity == 0 && it.notAttendanceAt?.hour == 7 && it.notAttendanceAt?.minute == 41 }

        Assert.assertNotNull(courseStudentAttendanceToAssert)
        Assert.assertEquals(0, courseStudentAttendanceToAssert?.quantity)
        Assert.assertEquals(7, courseStudentAttendanceToAssert?.notAttendanceAt?.hour)
        Assert.assertEquals(41, courseStudentAttendanceToAssert?.notAttendanceAt?.minute)
    }

    @Test
    fun failureHasBeenAlreadyRegisteredTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_FAILURE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "not_attendance_at" :{
            "hour": "7",
            "minute" : "41"
        },
        "day_of_week":"TUESDAY",
        "created_at": "2021-04-09 05:00:00"
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("This failure has already been registered.")))
    }

    @Test
    fun courseScheduleDateDoesNotMatchTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_FAILURE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "not_attendance_at" :{
            "hour": "1",
            "minute" : "41"
        },
        "day_of_week":"TUESDAY",
        "created_at": "2021-04-10 05:00:00"
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The course schedule does not match with this date time.")))
    }

    @Test
    fun courseStudentDoesNotExistTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_FAILURE),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "12"
        val courseId = "2020"

        val jsonBody =
            """{
        "not_attendance_at" :{
            "hour": "7",
            "minute" : "41"
        },
        "day_of_week":"TUESDAY",
        "created_at": "2021-04-10 05:00:00"
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course's student does not exist.")))
    }

    @Test
    fun createCourseStudentGradeTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "course_evaluation_id":"1",
        "new_grade":2.0
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val courseStudentQualificationToAssert = courseStudentRepository.findByStudentIdAndCourseId(studentId, courseId)
            ?.courseStudentQualification?.find { it.courseEvaluationId == "1" }

        Assert.assertNotNull(courseStudentQualificationToAssert)
        Assert.assertEquals(2.0, courseStudentQualificationToAssert?.value)
    }

    @Test
    fun evaluationHasNotBeenRegisteredYetTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentGrade.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "2"
        val courseId = "2020"

        val jsonBody =
            """{
        "course_evaluation_id":"1",
        "new_grade":2.0
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Evaluation has not been registered yet.")))
    }

    @Test
    fun evaluationIsRepeatedTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentGrade.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "3"
        val courseId = "2020"

        val jsonBody =
            """{
        "course_evaluation_id":"2",
        "new_grade":2.0
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The evaluation has been assigned already.")))
    }

    @Test
    fun gradesIsToHighTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "course_evaluation_id":"1",
        "new_grade":6.0
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Grades must be between 0 and 5.")))
    }

    @Test
    fun gradesIsToLowTest() {

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.REGISTER_STUDENT_GRADE),
            groups = listOf(
                Groups.CLASS_TEACHER,
            )
        )

        fillCollection("course_students", "/testData/courseControllerTest/createCourseStudentFailure.json")
        fillCollection("courses", "/testData/courseControllerTest/createCourseStudentFailureCourse.json")

        val studentId = "1"
        val courseId = "2020"

        val jsonBody =
            """{
        "course_evaluation_id":"1",
        "new_grade":-1.0
        }"""

        val request = MockMvcRequestBuilders.post("${Routes.PATH_COURSES}/${Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID}", courseId, studentId)
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Grades must be between 0 and 5.")))
    }

    @Test
    fun editCourseStudentCalificationAndEvaluationDelivered() {
        fillCollection("course_students", "/testDatacourseStudentController/courseStudent.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "evaluation_id" : "2",
    "value" : 5,
    "created_at" : "2021-04-12 05:00:00" 
        }"""

        val request = MockMvcRequestBuilders.put(
            "${Routes.PATH_COURSES}/${Routes.PATH_EDIT_COURSE_STUDENT_CALIFICATION}",
            "2020",
            "1"
        )
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The evaluation has been assigned already.")))
    }

    @Test
    fun editCourseStudentCalificationAndCourseDoesnotExist() {
        fillCollection("course_students", "/testDatacourseStudentController/courseStudent.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "evaluation_id" : "1",
    "value" : 5,
    "created_at" : "2021-04-12 05:00:00" 
        }"""

        val request = MockMvcRequestBuilders.put(
            "${Routes.PATH_COURSES}/${Routes.PATH_EDIT_COURSE_STUDENT_CALIFICATION}",
            "2023",
            "1"
        )
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Course not found")))
    }

    @Test
    fun editCourseStudentCalificationAndCourseStudentDoesnotExist() {
        fillCollection("course_students", "/testDatacourseStudentController/courseStudent.json")
        fillCollection("courses", "/testDatacourseProfessorLogControllerTest/courses.json")

        val jsonBody =
            """{
        "evaluation_id" : "1",
    "value" : 5,
    "created_at" : "2021-04-12 05:00:00" 
        }"""

        val request = MockMvcRequestBuilders.put(
            "${Routes.PATH_COURSES}/${Routes.PATH_EDIT_COURSE_STUDENT_CALIFICATION}",
            "2020",
            "30"
        )
            .content(jsonBody)
            .contentType(MediaType.APPLICATION_JSON)
        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student course not found")))
    }
}
