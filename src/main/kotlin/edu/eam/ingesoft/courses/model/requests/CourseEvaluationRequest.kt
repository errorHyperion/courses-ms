package edu.eam.ingesoft.courses.model.requests

data class CourseEvaluationRequest(
    val weight: Double,

    val name: String
)
