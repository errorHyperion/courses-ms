package edu.eam.ingesoft.courses.model.enums

enum class DayTimeEnum {
    DAY, NIGHT,
}
