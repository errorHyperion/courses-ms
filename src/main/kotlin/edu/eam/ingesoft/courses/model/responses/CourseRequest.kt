package edu.eam.ingesoft.courses.model.responses

import edu.eam.ingesoft.courses.model.enums.DayTimeEnum

data class CourseRequest(
    val id: String?,

    val group: String? = "A",

    val professorId: String,

    val academicSpaceId: String,

    val dayTime: DayTimeEnum? = DayTimeEnum.DAY
)
