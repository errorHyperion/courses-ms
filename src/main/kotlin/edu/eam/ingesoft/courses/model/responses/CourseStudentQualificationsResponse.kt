package edu.eam.ingesoft.courses.model.responses

data class CourseStudentQualificationsResponse(
    val id: String,
    val qualification: List<StudentQualificationResponse>,
    val finalNote: Double
)
