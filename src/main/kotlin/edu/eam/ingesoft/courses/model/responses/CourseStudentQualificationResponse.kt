package edu.eam.ingesoft.courses.model.responses

data class CourseStudentQualificationResponse(
    val course: CourseResponse,
    val qualification: List<QualificationResponse>,
    val finalNote: Double
)
