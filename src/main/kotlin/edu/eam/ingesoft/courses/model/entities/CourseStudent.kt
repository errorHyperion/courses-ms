package edu.eam.ingesoft.courses.model.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "course_students")
data class CourseStudent(
    @Id
    val id: String?,

    val studentId: String,

    val courseId: String,

    val courseStudentQualification: MutableList<CourseStudentQualification>?,

    val courseStudentAttendance: MutableList<CourseStudentAttendance>?,
)
