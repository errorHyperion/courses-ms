package edu.eam.ingesoft.courses.model.entities

data class Time(
    val hour: Int,
    val minute: Int
)
