package edu.eam.ingesoft.courses.model.requests

import edu.eam.ingesoft.courses.model.entities.Time
import java.time.DayOfWeek
import java.util.Date

data class CreateCourseStudentAttendanceRequest(
    val notAttendanceAt: Time,
    val dayOfWeek: DayOfWeek,
    val createdAt: Date,
    val quantity: Int
)
