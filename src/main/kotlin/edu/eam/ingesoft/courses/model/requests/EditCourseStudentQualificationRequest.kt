package edu.eam.ingesoft.courses.model.requests

import java.util.Date

data class EditCourseStudentQualificationRequest(
    val evaluationId: String,
    val value: Double,
    val createdAt: Date
)
