package edu.eam.ingesoft.courses.model

import edu.eam.ingesoft.courses.model.entities.Time
import java.time.DayOfWeek
import java.util.Date

data class EditProfessorLogRequest(
    val description: String,

    val hours: Int,

    val createdAt: Date,

    val logDate: Time,

    val dayOfWeek: DayOfWeek
)
