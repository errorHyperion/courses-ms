package edu.eam.ingesoft.courses.model.requests

data class CreateCourseStudentQualificationRequest(
    val courseEvaluationId: String,
    val newGrade: Double
)
