package edu.eam.ingesoft.courses.model.entities

import org.springframework.format.annotation.DateTimeFormat
import java.util.Date

data class CourseStudentQualification(
    var courseEvaluationId: String?,

    var value: Double? = 0.0,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    val createdAt: Date? = Date(),

    var delivered: Boolean? = false
)
