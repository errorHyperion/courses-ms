package edu.eam.ingesoft.courses.config

object Groups {
    const val SYSTEM_ADMINISTRATOR = "systemAdministrator"
    const val PROGRAM_DIRECTOR = "programDirector"
    const val REGISTRATION_AND_CONTROL = "registrationAndControl"
    const val CLASS_TEACHER = "classTeacher"
    const val REGISTRATION_ADN_CONTROL = "registrationAndControl"
    const val DELETE_TEACHER_LOG = "deleteTeacherLog"
    const val EDIT_TEACHER_LOG = "editTeacherLog"
}
