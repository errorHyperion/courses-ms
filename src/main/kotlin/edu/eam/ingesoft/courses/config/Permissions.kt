package edu.eam.ingesoft.courses.config

object Permissions {
    const val CREATE_COURSE_EVALUATION = "createCourseEvaluation"
    const val FIND_QUALIFICATION_BY_COURSE = "findQualificationByCourse"
    const val REGISTER_STUDENT_GRADE = "registerStudentGrade"
    const val DELIVER_STUDENT_GRADE = "deliverStudentGrade"
    const val ADD_COURSE_SCHEDULE = "addCourseSchedule"
    const val REGISTER_TEACHER_LOG = "registerTeacherLog"
    const val CREATE_COURSE = "createCourse"

    const val FIND_QUALIFICATIONS_BY_STUDENT = "findQualificationsByStudent"
    const val REGISTER_STUDENT_FAILURE = "registerStudentFailure"
    const val DELETE_TEACHER_LOG = "deleteTeacherLog"
    const val EDIT_TEACHER_LOG = "editTeacherLog"
}
