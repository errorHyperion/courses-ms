package edu.eam.ingesoft.courses.controllers

import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.model.EditProfessorLogRequest
import edu.eam.ingesoft.courses.model.entities.Course
import edu.eam.ingesoft.courses.model.entities.CourseSchedule
import edu.eam.ingesoft.courses.model.requests.CourseEvaluationRequest
import edu.eam.ingesoft.courses.model.requests.EditCourseStudentQualificationRequest
import edu.eam.ingesoft.courses.model.responses.CourseRequest
import edu.eam.ingesoft.courses.security.Secured
import edu.eam.ingesoft.courses.services.CourseEvaluationService
import edu.eam.ingesoft.courses.services.CourseProfessorLogService
import edu.eam.ingesoft.courses.services.CourseService
import edu.eam.ingesoft.courses.services.CourseStudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.PATH_COURSES)
class CourseController {

    @Autowired
    lateinit var courseService: CourseService

    @Autowired
    lateinit var courseEvaluationService: CourseEvaluationService

    @Secured(
        permissions = [Permissions.CREATE_COURSE],
        groups = [Groups.PROGRAM_DIRECTOR, Groups.REGISTRATION_ADN_CONTROL, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping
    fun createCourse(@RequestBody courseRequest: CourseRequest) = courseService.createCourse(courseRequest)

    @Autowired
    lateinit var courseStudent: CourseStudentService

    @Autowired
    lateinit var professorLogService: CourseProfessorLogService

    @Autowired
    lateinit var courseStudentService: CourseStudentService

    @GetMapping(Routes.PATH_FIND_COURSE_BY_ID)
    fun findCourseById(@PathVariable courseId: String) = courseService.findCourseById(courseId)

    @PutMapping(Routes.PATH_EDIT_COURSE_BY_ID)
    fun editCourse(@PathVariable courseId: String, @RequestBody course: Course) =
        courseService.editCourse(courseId, course)

    @Secured(permissions = [Permissions.ADD_COURSE_SCHEDULE], groups = [Groups.PROGRAM_DIRECTOR, Groups.REGISTRATION_AND_CONTROL, Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping(Routes.SCHEDULE_PATH)
    fun createCourseSchedule(@PathVariable("courseId") courseId: String, @RequestBody schedule: CourseSchedule) =
        courseService.createSchedule(courseId, schedule)

    @Secured(permissions = [Permissions.CREATE_COURSE_EVALUATION], groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR])
    @PostMapping(Routes.PATH_ADD_EVALUATION_COURSE)
    fun createCourseEvaluation(
        @PathVariable courseId: String,
        @RequestBody courseEvaluationRequest: CourseEvaluationRequest
    ) =
        courseEvaluationService.createCourseEvaluation(
            courseId,
            courseEvaluationRequest.name,
            courseEvaluationRequest.weight
        )

    @Secured(
        permissions = [Permissions.FIND_QUALIFICATIONS_BY_STUDENT],
        groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR]
    )
    @GetMapping(Routes.PATH_FIND_QUALIFICATIONS_BY_STUDENT)
    fun findQualificationsByStudent(@PathVariable studentCode: String) =
        courseService.findQualificationsByStudent(studentCode)

    @Secured(permissions = [Permissions.DELIVER_STUDENT_GRADE], groups = [Groups.CLASS_TEACHER])
    @PatchMapping(Routes.DELIVER_GRADE_PATH)
    fun deliverGrade(
        @PathVariable("courseId") courseId: String,
        @PathVariable("studentId") studentId: String,
        @PathVariable("evaluationId") evaluationId: String
    ) = courseStudent.deliverGreadStudent(courseId, studentId, evaluationId)

    @Secured(
        permissions = [Permissions.EDIT_TEACHER_LOG],
        groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PutMapping(Routes.EDIT_PROFESSOR_LOG)
    fun editProfessorLog(
        @PathVariable courseId: String,
        @PathVariable id: String,
        @RequestBody request: EditProfessorLogRequest
    ) =
        professorLogService.editCourseProfessorLog(
            id,
            request.description,
            courseId,
            request.hours,
            request.createdAt,
            request.logDate,
            request.dayOfWeek
        )

    @Secured(
        permissions = [Permissions.DELETE_TEACHER_LOG],
        groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR]
    )
    @DeleteMapping(Routes.DELETE_PROFESSOR_LOG)
    fun deleteProfessorLog(@PathVariable courseId: String, @PathVariable id: String) =
        professorLogService.delete(id, courseId)

    @Secured(permissions = [Permissions.FIND_QUALIFICATION_BY_COURSE], groups = [Groups.CLASS_TEACHER, Groups.REGISTRATION_AND_CONTROL, Groups.PROGRAM_DIRECTOR, Groups.SYSTEM_ADMINISTRATOR])
    @GetMapping(Routes.PATH_FIND_QUALIFICATION_BY_COURSE)
    fun findQualificationsByCourse(@PathVariable courseId: String, pageable: Pageable) =
        courseService.findQualificationsByCourse(courseId, pageable)

    @PutMapping(Routes.PATH_EDIT_COURSE_STUDENT_CALIFICATION)
    fun editCalification(
        @PathVariable courseId: String,
        @PathVariable studentId: String,
        @RequestBody requests: EditCourseStudentQualificationRequest
    ) = courseStudentService.editCalification(
        courseId,
        studentId,
        requests.evaluationId,
        requests.value,
        requests.createdAt
    )
}
