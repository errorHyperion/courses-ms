package edu.eam.ingesoft.courses.controllers

import edu.eam.ingesoft.courses.config.Groups
import edu.eam.ingesoft.courses.config.Permissions
import edu.eam.ingesoft.courses.config.Routes
import edu.eam.ingesoft.courses.model.requests.CreateCourseStudentAttendanceRequest
import edu.eam.ingesoft.courses.model.requests.CreateCourseStudentQualificationRequest
import edu.eam.ingesoft.courses.security.Secured
import edu.eam.ingesoft.courses.services.CourseStudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping(Routes.PATH_COURSES)
@RestController
class CourseStudentController {

    @Autowired
    lateinit var courseStudentService: CourseStudentService

    @Secured(
        permissions = [Permissions.REGISTER_STUDENT_FAILURE],
        groups = [Groups.CLASS_TEACHER, Groups.SYSTEM_ADMINISTRATOR]
    )
    @PostMapping(Routes.ADD_FAILURE_BY_COURSE_ID_AND_COURSE_STUDENT_ID)
    fun createFailure(@PathVariable studentId: String, @PathVariable courseId: String, @RequestBody request: CreateCourseStudentAttendanceRequest) =
        courseStudentService.createFailure(studentId, courseId, request.notAttendanceAt, request.dayOfWeek, request.createdAt, request.quantity)

    @Secured(permissions = [Permissions.REGISTER_STUDENT_GRADE], groups = [Groups.CLASS_TEACHER])
    @PostMapping(Routes.ADD_GRADE_BY_COURSE_ID_AND_COURSE_STUDENT_ID)
    fun createGrade(@PathVariable studentId: String, @PathVariable courseId: String, @RequestBody request: CreateCourseStudentQualificationRequest) =
        courseStudentService.createGrade(studentId, request.courseEvaluationId, courseId, request.newGrade)
}
