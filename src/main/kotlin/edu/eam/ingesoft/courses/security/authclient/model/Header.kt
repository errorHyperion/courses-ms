package edu.eam.ingesoft.courses.security.authclient.model

data class Header(
    val name: String,
    val bearer: String,
)
