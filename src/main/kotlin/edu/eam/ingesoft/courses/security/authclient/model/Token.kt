package edu.eam.ingesoft.courses.security.authclient.model

data class Token(
    val token: String,
)
