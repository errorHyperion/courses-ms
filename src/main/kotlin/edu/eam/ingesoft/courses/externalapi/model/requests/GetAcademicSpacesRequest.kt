package edu.eam.ingesoft.courses.externalapi.model.requests

data class GetAcademicSpacesRequest(
    val ids: List<Long>
)
