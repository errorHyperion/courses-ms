package edu.eam.ingesoft.courses.externalapi.model.responses

data class AcademicSpaceResponse(
    val id: Long,
    val name: String
)
