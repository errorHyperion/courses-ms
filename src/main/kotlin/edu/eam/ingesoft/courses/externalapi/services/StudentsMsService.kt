package edu.eam.ingesoft.courses.externalapi.services

import edu.eam.ingesoft.courses.externalapi.client.StudentsMsClient
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.EntityNotFoundException

@Component
class StudentsMsService {

    @Autowired
    lateinit var studentsMsClient: StudentsMsClient

    fun findStudentByCode(code: String) {
        try {
            studentsMsClient.getStudentByCode(code)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Student Not Found")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception")
        }
    }
}
