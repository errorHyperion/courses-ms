package edu.eam.ingesoft.courses.externalapi.client

import edu.eam.ingesoft.courses.externalapi.model.responses.StudentResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "StudentsMsClient", url = "\${externalapi.students-ms}/api/students-ms/")
interface StudentsMsClient {
    @RequestMapping(method = [RequestMethod.GET], path = ["/students/{code}"])
    fun getStudentByCode(@PathVariable code: String): StudentResponse
}
