package edu.eam.ingesoft.courses.externalapi.clients

import edu.eam.ingesoft.courses.externalapi.model.requests.GetAcademicSpacesRequest
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicPeriodResponse
import edu.eam.ingesoft.courses.externalapi.model.responses.AcademicSpaceResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient(name = "AcademicMsClient", url = "\${externalapi.academic-ms}/api/academic-ms")
interface AcademicMsClient {
    @RequestMapping(method = [RequestMethod.GET], path = ["/academic-spaces/{idAcademicSpace}"])
    fun getAcademicSpace(@PathVariable idAcademicSpace: String): AcademicSpaceResponse

    @RequestMapping(method = [RequestMethod.GET], path = ["/academic-periods/current"])
    fun getAcademicPeriod(): AcademicPeriodResponse

    @RequestMapping(method = [RequestMethod.POST], path = ["/academic-spaces/get-by-ids/"])
    fun getAcademicSpacesByIds(@RequestBody getAcademicSpacesRequest: GetAcademicSpacesRequest): List<AcademicSpaceResponse>
}
