package edu.eam.ingesoft.courses.repositories

import edu.eam.ingesoft.courses.model.entities.CourseStudent
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import java.util.Date

@Repository
interface CourseStudentRepository : MongoRepository<CourseStudent, String> {
    fun findByStudentId(studentId: String): List<CourseStudent>

    @Query("{studentId:?0,courseId:?1,'courseStudentAttendance.createdAt':?2,'courseStudentAttendance.notAttendanceAt.hour':?3,'courseStudentAttendance.notAttendanceAt.minute':?4}")
    fun findCourseStudentByIdAndDate(studentId: String, courseId: String, createdAt: Date, hour: Int, minute: Int): CourseStudent?

    @Query("{studentId:?0,courseId:?1,'courseStudentQualification.courseEvaluationId':?2}")
    fun findEvaluationByIdAndCourse(studentId: String, courseId: String, evaluationId: String): CourseStudent?

    @Query("{studentId:?0,courseId:?1}")
    fun findByStudentIdAndCourseId(studentId: String, courseId: String): CourseStudent?

    @Query("{courseId: ?0, studentId: ?1}")
    fun findStudentInCourse(courseId: String, studentId: String): CourseStudent?

    @Query("{studentId :?0, courseId :?1, 'courseStudentQualification.courseEvaluationId' :?2}")
    fun findCourseStudentQualification(studentId: String, courseId: String, courseEvaluationId: String): CourseStudent?

    fun findByCourseId(id: String, pageable: Pageable?): Page<CourseStudent>
}
