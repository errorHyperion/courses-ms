package edu.eam.ingesoft.courses.repositories

import edu.eam.ingesoft.courses.model.entities.CourseProfessorLog
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface CourseProfessorLogRepository : MongoRepository<CourseProfessorLog, String> {
    @Query("{_id:?0}")
    fun findByIdAndCourseId(id: String): CourseProfessorLog?

    fun findByCourseId(courseId: String): CourseProfessorLog?
}
